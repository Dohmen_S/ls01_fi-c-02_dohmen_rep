//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package arrayUebungenMitFunktionen;
import java.util.Scanner;

public class ArrayUebungenMitFunktionen {
	public static int[] getArrayFromUser(Scanner UserEingabe, int arraySize) {
		int array[] = new int[arraySize];
		System.out.printf("Bitte geben Sie eine Zahl ein.\n");
		array[0] = UserEingabe.nextInt();
		for (int x=1; x < arraySize; x++) {
			System.out.printf("Bitte geben Sie noch eine Zahl ein.\n");
			array[x] = UserEingabe.nextInt();
		}
		return array;
	}
	
	public static String convertArrayToString(int[] zahlen) {
		String formatierteZahl = "";
		String formatierteListe = String.valueOf(zahlen[0]);
		for (int x=1; x < zahlen.length; x++) {
			formatierteZahl = String.valueOf(zahlen[x]);
			formatierteListe = formatierteListe + ", " + formatierteZahl;
		}
		return formatierteListe;
	}
	
	public static void aufgabe1(Scanner UserEingabe) {
		int array[] = getArrayFromUser(UserEingabe, 4);
		String convertedArray = convertArrayToString(array);
		System.out.printf("Hier ist die Liste an Zahlen:\n%s\n", convertedArray);
	}
	
	public static void aufgabe2(Scanner UserEingabe) {
		int array[] = getArrayFromUser(UserEingabe, 1);
		String convertedArray = String.valueOf(array[0]);
		char fragmentedString[] = new char[convertedArray.length()];
		String finalString = "";
		for (int x=0, y=convertedArray.length(); x < convertedArray.length();x++, y--) {
			fragmentedString[x] = convertedArray.charAt(y-1);
		}
		for (int x=0; x < convertedArray.length();x++) {
			finalString = finalString + fragmentedString[x];
		}
		System.out.printf("Hier ist diese Zahl in umgekehrt:\n%s\n", finalString);
	}
	
	public static void main (String[]args) {
		String aufgabenWahl = "";
		Scanner Tastatur = new Scanner(System.in);
		boolean programmAktiv = true;
		while (programmAktiv) {
			System.out.println("W�hlen Sie eine Aufgabe aus. M�gliche Eingaben sind: aufgabe1, aufgabe2, aufgabe3. Um das Programm zu beenden geben Sie bitte ende ein.");
			aufgabenWahl = Tastatur.next();
			if (aufgabenWahl.equals("aufgabe1")) {
				aufgabe1(Tastatur);
			}
			else if (aufgabenWahl.equals("aufgabe2")) {
				aufgabe2(Tastatur);
			}
			else if (aufgabenWahl.equals("ende")) {
				programmAktiv = false;
				System.out.println("Das Programm wird beendet.");
			}
			else {
				System.out.println("Eingabe unverst�ndlich, bitte wiederholen Sie die Eingabe.");
			}
		}
	}
}

//Sergey Dohmen und Josua Kawgan-Kagan
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package millionaer;
import java.util.Scanner;

public class Millionaer {
	public static void main(String[]args) {
		
		Scanner Tastatur = new Scanner(System.in);
		char repeat = 'y';
		
		while (repeat == 'y' || repeat == 'Y') {
			double einlage = 100;
			double zinsSatz = 2;
			System.out.println("Bitte geben Sie ein, wie viel Geld Sie anlegen.");
			einlage = Tastatur.nextDouble();
			System.out.println("Bitte geben Sie ein, wie hoch der Zinssatz ist.");
			zinsSatz = Tastatur.nextDouble();
			
			System.out.printf("Es dauert %d Jahre\n", zeitBerechnung(einlage, zinsSatz));
			
			System.out.println("Wollen Sie den Vorgang nochmal wiederholen? Geben Sie 'y' ein um den Vorgang zu wiederholen oder 'n' um den Vorgang abzubrechen.");
			repeat = Tastatur.next().charAt(0);
			if (repeat != 'n' && repeat != 'N')
				System.out.println("Das Programm wird wie gew�nscht beendet.");
			else if (repeat != 'y' && repeat != 'Y' && repeat != 'n' && repeat != 'N')
				System.out.println("Sie haben weder 'y' noch 'n' eingegeben, das Programm wird abgebrochen.");
		}
	}
	
	static int zeitBerechnung(double einlage, double zinsSatz) {
		int zaehler = 0;
		while (einlage<1000000) {
			einlage = einlage * (zinsSatz/100+1);
			zaehler++;
		}
		return zaehler;	
	}

}

//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package primzahl;
import java.util.Scanner;

public class Primzahl {
	public static void main(String[]args) {
		Scanner Tastatur = new Scanner(System.in);
		
		System.out.printf("Geben Sie bitte eine Zahl ein um zu pr�fen ob es eine Primzahl ist.\n");
		int zahl = Tastatur.nextInt();
		
		if (primzahlUeberpruefung(zahl))
			System.out.printf("%d ist eine Primzahl.\n", zahl);
		else
			System.out.printf("%d ist keine Primzahl.\n", zahl);
	}
	
	static boolean primzahlUeberpruefung(int zahl) {
		int zaehler = 2;
		int zahlerMax = zahl/2;
		boolean answer = true;
		while (zaehler<zahlerMax) {
			if (zahl%zaehler==0 && zahl!=zaehler);
				answer = false;
			zaehler++;
		}
		
		return answer;
	}

}

//Sergey Dohmen
//Arbeitsauftrag-LS01-5-printf-formatieren
//Version 2.0, reupload
package aufgaben;

//Sergey Dohmen

public class Aufgabe3 {

	public static void main(String[] args) {
		int[] fahrenheit = {-20, -10, 0, 20, 30};
		double[] celsius = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
		String vorzeichen=" ";
		System.out.printf("%-12s |%10s\n", "Fahrenheit", "Celsius");
		System.out.printf("- - - - - - - - - - - -\n");
		for (int n=0; n<=4; n++) 
		{
			if (fahrenheit[n]>=0)
				vorzeichen="+";
			else 
			{
				vorzeichen="-";
				fahrenheit[n]=fahrenheit[n]*-1;
			}
				
			System.out.printf("%s%-12d|%10.2f\n", vorzeichen, fahrenheit[n], celsius[n]);
			vorzeichen="";
		}
	}
	

}

//Sergey Dohmen
//Arbeitsauftrag-LS01-5-printf-formatieren
//Version 2.0, reupload
package aufgaben;

//Sergey Dohmen

public class Aufgabe2 {

	public static void main(String[] args) {
		for (int n=0; n<=5; n++)
		{
			String anfangsausgabe = n+"!";
			//System.out.printf("%d!=", n);
			int b=1;
			String zwischenausgabe = "=";
			for (int a=0; a<=n; a++) 
			{
				if (a!=0)
						{
							b=b*a;
							if (a!=n) {
								//System.out.printf(" %d * ", a);
								zwischenausgabe=zwischenausgabe+" "+a+" *";
							}
							else {
								//System.out.printf(" %d ", a);
								zwischenausgabe=zwischenausgabe+" "+a;
							}
						}
			}
			String endausgabe = "= "+b+'\n';
			System.out.printf("%-5s %-19s %-4s", anfangsausgabe, zwischenausgabe, endausgabe, b);
		}
	}
	

}

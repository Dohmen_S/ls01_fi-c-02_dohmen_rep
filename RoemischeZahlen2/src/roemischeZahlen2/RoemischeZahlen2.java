//Sergey Dohmen und Josua Kawgan-Kagan
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package roemischeZahlen2;
import java.util.Scanner;

public class RoemischeZahlen2 {

	public static void main(String[] args) {
		System.out.printf("Bitte geben Sie eine r�mische Zahl ein.\n");
		Scanner Tastatur = new Scanner(System.in);
		
		String roemZahl = Tastatur.next(); //.next statt .nextString
		int roemDezimal = roemZuDezimal(roemZahl);
		if (roemDezimal!=0)
			System.out.printf("Diese r�mische Zahl ist eine %d\n", roemDezimal);
		else
			System.out.printf("Das ist keine R�mische Zahl.\n");

	}
	
	static int roemZuDezimal(String roemZahl) {
		int zaehler = 0;
		int ausgabeZahl = 0;
		
		while (zaehler<roemZahl.length()) {
			if (roemZahl.charAt(zaehler)==('I'))
				if ((zaehler+1)<roemZahl.length() && (roemZahl.charAt(zaehler+1)==('V') || roemZahl.charAt(zaehler+1)==('X') || roemZahl.charAt(zaehler+1)==('L') || roemZahl.charAt(zaehler+1)==('C') || roemZahl.charAt(zaehler+1)==('D') || (roemZahl.charAt(zaehler+1)==('M'))))

					ausgabeZahl -= 1;
				else
					ausgabeZahl += 1;
			else if (roemZahl.charAt(zaehler)==('V'))
				if ((zaehler+1)<roemZahl.length() && (roemZahl.charAt(zaehler+1)==('X') || roemZahl.charAt(zaehler+1)==('L') || roemZahl.charAt(zaehler+1)==('C') || roemZahl.charAt(zaehler+1)==('D') || (roemZahl.charAt(zaehler+1)==('M'))))
					ausgabeZahl -= 5;
				else
					ausgabeZahl += 5;
			else if (roemZahl.charAt(zaehler)==('X'))
				if ((zaehler+1)<roemZahl.length() && (roemZahl.charAt(zaehler+1)==('L') || roemZahl.charAt(zaehler+1)==('C') || roemZahl.charAt(zaehler+1)==('D') || (roemZahl.charAt(zaehler+1)==('M'))))
					ausgabeZahl -= 10;
				else
					ausgabeZahl += 10;
			else if (roemZahl.charAt(zaehler)==('L'))
				if ((zaehler+1)<roemZahl.length() && (roemZahl.charAt(zaehler+1)==('C') || roemZahl.charAt(zaehler+1)==('D') || (roemZahl.charAt(zaehler+1)==('M'))))
					ausgabeZahl -= 50;
				else
					ausgabeZahl += 50;
			else if (roemZahl.charAt(zaehler)==('C'))
				if ((zaehler+1)<roemZahl.length() && (roemZahl.charAt(zaehler+1)==('D') || (roemZahl.charAt(zaehler+1)==('M'))))
					ausgabeZahl -= 100;
				else
					ausgabeZahl += 100;
			else if (roemZahl.charAt(zaehler)==('D'))
				if ((zaehler+1)<roemZahl.length() && roemZahl.charAt(zaehler+1)==('M'))
					ausgabeZahl -= 500;
				else
					ausgabeZahl += 500;
			else if (roemZahl.charAt(zaehler)==('M'))
				ausgabeZahl += 1000;
			zaehler++;
		}
		
		return ausgabeZahl;
	}

}

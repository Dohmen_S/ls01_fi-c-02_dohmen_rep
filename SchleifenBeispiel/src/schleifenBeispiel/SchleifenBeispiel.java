//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package schleifenBeispiel;
import java.util.Scanner;

public class SchleifenBeispiel {

	public static void main(String[] args) {
		Scanner Tastatur = new Scanner(System.in);
		
		int a = 1;
		
		while (a!=0) {
			System.out.println("Bitte geben Sie eine beliebige Zahl, oder eine 0 zum beenden ein.");
			a = Tastatur.nextInt();
			System.out.printf("Sie haben eine %d eingegeben.\n", a);
		}
	}

}

//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package schleifenBeispiel;
import java.util.Scanner;

public class SumA {

	public static void main(String[]args) {
		Scanner myScanner = new Scanner(System.in);
		int zaehler = 1; 
		int erg = 0;
		
		System.out.println("Geben Sie bitte eine Zahl ein!");
		int n = myScanner.nextInt();
		
		while(zaehler<n) {
			erg += zaehler;
			zaehler ++;
		}
		
		System.out.printf("Ihr Ergebnis ist %d!", erg);
	}
}

package fuellstandsregelung;
import java.util.Scanner;

public class Fuellstandsregelung {	
	//public static simulation
	
	public static void main(String[]args) {
		Scanner Tastatur = new Scanner(System.in);
		String antwort = "y";
		
		while(antwort.charAt(0)=='y' || antwort.charAt(0)=='Y')
		{
			boolean untererFüllstandÜberschritten = false;
			boolean obererFüllstandÜberschritten = false;
			boolean filterVerschmutzt = false;
			
			boolean alarm = false;
			
			System.out.println("Ist der obere Füllstand überschritten?");
			obererFüllstandÜberschritten = Tastatur.nextBoolean();
			
			System.out.println("Ist der untere Füllstand überschritten?");
			untererFüllstandÜberschritten = Tastatur.nextBoolean();
			
			System.out.println("Ist die Pumpe verschmutzt?");
			filterVerschmutzt = Tastatur.nextBoolean();
			
			if (obererFüllstandÜberschritten == false && filterVerschmutzt == false)
				System.out.println("Pumpe wird in Betrieb genommen.");
			else if ((obererFüllstandÜberschritten == false && filterVerschmutzt == true) ||
					untererFüllstandÜberschritten == false ||
					untererFüllstandÜberschritten == false && obererFüllstandÜberschritten == true )
				alarm = true;
			
			if (alarm == true)
				System.out.println("Alarm!");
			
			System.out.println("Wollen Sie die Simulation fortfahren lassen? y oder n?");
			antwort = Tastatur.next();
		}
	}
}

//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
//Version 2.0, reupload
//Aufgabe LS-01-10 (enth�lt LS-01-8)
package fahrkartenautomat04;

import java.util.Scanner;

public class Fahrkartenautomat04
{
	public static int umrechnungZuCent (double wert) {
		wert *= 100;
		int ergebnis = (int) wert;
		return ergebnis;
	}
	
	public static double umrechnungZuEuro (int wert) {
		double ergebnis = (int) wert;
		ergebnis /= 100;
		return ergebnis;
	}	
	
	public static int fahrkartenBestellung(Scanner eingabe) {
       System.out.print("Zu zahlender Betrag (EURO): ");
       double ticketpreis = eingabe.nextDouble();
       System.out.print("Anzahl der Tickets: ");
       int anzahlTickets = eingabe.nextInt();
       int zuZahlenderBetrag = umrechnungZuCent(ticketpreis) * anzahlTickets;
       return zuZahlenderBetrag;
	}
	
	public static int fahrkartenBezahlen(Scanner eingabe, int zuZahlenderBetrag) {
       int eingezahlterGesamtbetrag = 0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro\n", umrechnungZuEuro(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   double eingeworfeneM�nze = eingabe.nextDouble();
           eingezahlterGesamtbetrag += umrechnungZuCent(eingeworfeneM�nze);
       }
       return eingezahlterGesamtbetrag;

	}
	
	public static void rueckgeldAusgeben(int eingezahlterGesamtbetrag, int zuZahlenderBetrag) {
		int r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", umrechnungZuEuro(r�ckgabebetrag));
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 200) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 200;
	           }
	           while(r�ckgabebetrag >= 100) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 100;
	           }
	           while(r�ckgabebetrag >= 50) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 50;
	           }
	           while(r�ckgabebetrag >= 20) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 20;
	           }
	           while(r�ckgabebetrag >= 10) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 10;
	           }
	           while(r�ckgabebetrag >= 5)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 5;
	           }
	       }
	}
	
	public static void fahrscheinAusgeben () {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       int zuZahlenderBetrag; //in Cent
       int eingezahlterBetrag; //in Cent
       
       zuZahlenderBetrag = fahrkartenBestellung(tastatur);
       
       eingezahlterBetrag = fahrkartenBezahlen (tastatur, zuZahlenderBetrag);
       
       rueckgeldAusgeben(eingezahlterBetrag, zuZahlenderBetrag);
       
       fahrscheinAusgeben ();
    }
    
    
}
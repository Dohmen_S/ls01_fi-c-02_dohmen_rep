//Sergey Dohmen und Josua Kawgan-Kagan
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package methodenBeispiel;

public class MethodenBeispiel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 40;
		int y = 30;
		test();
		vergleich(x, y);
		int erg2 = max(7, 6, 8);
		System.out.println("Die gr��te Zahl ist " + erg2);
	}
	
	public static void test() {
		System.out.println("Test");
	}
	
	public static void vergleich(int a, int b) {
		if (a<b)
			System.out.printf(a + " ist kleiner als " + b + " .\n");
		else
			System.out.printf(a + " ist gr��er als " + b + " oder gleich gro�.\n");
	}
	
	public static int max(int a, int b, int c) {
		if (a<b) {
			if (b<c)
				return c;
			else
				return b;
		}
		else {
			if (a<c)
				return c;
			else
				return a;
		}
	}

}

//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/OOPSpeicherung/
package oopSpeicheung;
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Benutzerverwaltung {

    public static void main(String[] args) {
        try {
            BenutzerverwaltungV30.start();
        }
        catch (Exception e){
            System.out.println("Schief gelaufen");
        };
    }
}

class BenutzerverwaltungV30{
    private static BenutzerListe benutzerListe;

    public static void start() throws Exception{
        benutzerListe = new BenutzerListe();

        // Wir legen einige Beispielbenutzer an:
        benutzerListe.insert(new Benutzer("Admin", new char[]{36, 61, 72, 72, 75, 13, 14, 15}));
        benutzerListe.insert(new Benutzer("Paula", Crypto.encrypt("paula".toCharArray())));
        benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));

        // Wir speichern die serialisierten bin�ren Benutzerdaten in einer Bin�rdatei.
        // Eine solche Datei wird dann im Regelbetrieb beim Programmstart geladen.
        ObjectOutputStream benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerListe);
        benutzerdatei1.close();

        // Hier w�re dann der regul�re Programmstart, bei dem zuerst
        // die Benutzerdaten geladen werden:
        ObjectInputStream benutzerdatei = new ObjectInputStream(new FileInputStream("Benutzer.bin"));
        BenutzerListe benutzerListe = (BenutzerListe)benutzerdatei.readObject();
        benutzerdatei.close();
        System.out.println("-test->\n" + benutzerListe.select() + "<-test-\n");

        // Hier bitte das Men� mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einf�gen, sowie die entsprechenden Abl�ufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einf�gen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        Scanner tastatur = new Scanner(System.in);
        boolean systemL�uft = true;
        while(systemL�uft) {
            // Hier meldet sich der Benutzer an, z.B. Paula mit ihrem Passwort paula:
            // Versuchen Sie sich als Benutzer Admin anzumelden, indem Sie das Passwort knacken.
            System.out.print("Name: ");
            String inputName = tastatur.next();
            System.out.print("Passwort: ");
            String inputPasswort = tastatur.next();
            if (authenticate(inputName, Crypto.encrypt(inputPasswort.toCharArray()))) {
                System.out.println("Hallo " + inputName + "! Sie sind angemeldet.");
                // Arbeitsumgebung des Benutzers starten.
                // ...
                // Benutzer hat sich abgemeldet.
                System.out.println("Auf Wiedersehen.");
                System.out.print("System herunterfahren? [j/n] ");
                systemL�uft = !tastatur.next().equals("j");
            } else {
                System.out.println("Name oder Passwort falsch.");
            }
        }

        // Wir speichern hier in unserer Testversion die Benutzerdaten f�r den Fall,
        // dass Benutzer hinzugef�gt oder entfernt wurden.
        benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerListe);
        benutzerdatei1.close();
    }

    public static boolean authenticate(String name, char[] cryptoPw) {
        Benutzer b = benutzerListe.getBenutzer(name);
        if(b != null) {
            if(b.hasPasswort(cryptoPw)){
                return true;
            }
        }
        return false;
    }
}

class BenutzerListe implements Serializable {
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }

    public Benutzer getBenutzer(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b;
            }
            b = b.getNext();
        }
        return null;
    }

    public void insert(Benutzer b) {
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null) {
            first = last = b;
        }
        else {
            last.setNext(b);
            last = b;
        }
    }

    public String select() {
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }

    public String select(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name) {
        // ...
        return true;
    }
}

class Benutzer implements Serializable {
    private String name;
    private char[] passwort;  // Verschl�sselt!

    private Benutzer next;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public boolean hasPasswort(char[] cryptoPw){
        return Arrays.equals(this.passwort, cryptoPw);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}

class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }
}

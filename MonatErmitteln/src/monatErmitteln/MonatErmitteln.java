//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
//v 2.0
package monatErmitteln;
import java.util.Scanner;

public class MonatErmitteln {
	public static void main(String[]args) {
		System.out.printf("Bitte geben Sie eine Zahl zwischen 1 und 12 f�r den derzeitigen Monat ein\n");
		Scanner Tastatur = new Scanner(System.in);
		
		int a = Tastatur.nextInt();
		
		monatErmitteln(a);
	}
	
	static void monatErmitteln(int x) {
		if (x==1)
			System.out.printf("Es ist Januar");
		else if (x==2)
			System.out.printf("Es ist Februar");
		else if (x==3)
			System.out.printf("Es ist M�rz");
		else if (x==4)
			System.out.printf("Es ist April");
		else if (x==5)
			System.out.printf("Es ist Mai");
		else if (x==6)
			System.out.printf("Es ist Juni");
		else if (x==7)
			System.out.printf("Es ist Juli");
		else if (x==8)
			System.out.printf("Es ist August");
		else if (x==9)
			System.out.printf("Es ist September");
		else if (x==10)
			System.out.printf("Es ist Oktober");
		else if (x==11)
			System.out.printf("Es ist November");
		else if (x==12)
			System.out.printf("Es ist Dezember");
		else
			System.out.printf("Ung�ltiges Datum");
	}
}
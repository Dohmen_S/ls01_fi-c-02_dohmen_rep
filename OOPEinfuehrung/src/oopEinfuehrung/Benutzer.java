//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/OOPEinfuehrung/
package oopEinfuehrung;

public class Benutzer { 
	private String name;
	private String email;
	private String passwort;
	private String berechtigungsstatus;
	private boolean anmeldestatus;
	private String letztesLogindatum;
	
	public Benutzer(String name, String email, String passwort,String berechtigungsstatus) {
		this.name = name;
		this.email = email;
		this.passwort = passwort;
		this.berechtigungsstatus = berechtigungsstatus;
		this.anmeldestatus = false;
		this.letztesLogindatum = "Nie";
	}
	
	public String getName() {
		return this.name;
	}
	public String getEmail() {
		return this.email;
	}
	public String getPasswort() {
		return this.passwort;
	}
	public String getBerechtigungsstatus() {
		return this.berechtigungsstatus;
	}
	public boolean getAnmeldestatus() {
		return this.anmeldestatus;
	}
	public String getLetztesLogindatum() {
		return this.letztesLogindatum;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setEmail(String setEmail) {
		this.email = setEmail;
	}
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	public void setBerechtigungsstatus (String berechtigungsstatus) {
		this.berechtigungsstatus = berechtigungsstatus;
	}
	public void setAnmeldestatus(boolean anmeldestatus) {
		this.anmeldestatus = anmeldestatus;
	}
	public void setLetztesLogindatum(String letztesLogindatum) {
		this.letztesLogindatum = letztesLogindatum;
	}
	
	public void nutzerInformationen() {
		System.out.printf ("Folgende Daten stehen zur Verf�gung:\n");
		System.out.printf ("Name: %s\nE-Mail: %s\nPasswort: %s\nBerechtigungsstatus: %s\nAnmeldestatus: %b\nLetztesLogindatum: %s\n",
				this.getName(), this.getEmail(), this.getPasswort(), this.getBerechtigungsstatus(), 
				this.getAnmeldestatus(), this.getLetztesLogindatum());
	}
}
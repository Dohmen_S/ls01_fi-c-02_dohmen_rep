//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/OOPEinfuehrung/
package oopEinfuehrung;
//import oopEinfuehrung.Benutzer; unn�tiger import, weil Benutzer im selben package ist.
import java.util.Scanner;

import oopEinfuehrung.Benutzer;

public class OOPTest {

	public static void main(String[]args) {
		boolean testActive = true;
		Benutzer Testkandidat = new Benutzer("Max Mustermann", "m_mustermann@web.de", "1234", "Redakteur");
		Scanner Tastatur = new Scanner(System.in);
		
		while (testActive == true) {
			int gewaehlterVorgang;			
			
			System.out.printf("Tippen Sie eine Zahl ein um einen Vorgang durchzuf�hren,\n");
			System.out.printf("Sie haben folgende M�glichkeiten zu Auswahl:\n");
			System.out.printf("1.Werte ausgeben.\n");
			System.out.printf("2.Name ver�ndern.\n");
			System.out.printf("3.Email ver�ndern.\n");
			System.out.printf("4.Passwort ver�ndern.\n");
			System.out.printf("5.Berechtigungsstatus ver�ndern.\n");
			System.out.printf("6.Anmeldestatus ver�ndern.\n");
			System.out.printf("7.Letztes Logindatum ver�ndern.\n");
			System.out.printf("8.Programm beenden.\n");
			
			gewaehlterVorgang = Tastatur.nextInt();
			
			if (gewaehlterVorgang > 8 || gewaehlterVorgang < 1) {
				System.out.printf("Das war keine g�ltige Eingabe, bitte geben Sie eine Zahl zwischen 1 und 8 ein.\n");
			}
			else {
				switch(gewaehlterVorgang) {
					case 1:
						Testkandidat.nutzerInformationen();
						break;
					case 2:
						System.out.printf("Bitten geben Sie einen neuen Namen ein.\n");
						Testkandidat.setName(Tastatur.next());
						break;
					case 3:
						System.out.printf("Bitten geben Sie eine neue E-Mail Adresse ein.\n");
						Testkandidat.setEmail(Tastatur.next());
						break;
					case 4:
						System.out.printf("Bitten geben Sie ein neues Passwort ein.\n");
						Testkandidat.setPasswort(Tastatur.next());
						break;
					case 5:
						System.out.printf("Bitten geben Sie einen neuen Berechtigungsstatus ein.\n");
						Testkandidat.setBerechtigungsstatus(Tastatur.next());
						break;
					case 6:
						System.out.printf("Bitten geben Sie einen neuen Anmeldestatus ein. Bitte entweder True oder False eingeben.\n");
						Testkandidat.setAnmeldestatus(Tastatur.nextBoolean());
						break;
					case 7:
						System.out.printf("Bitten geben Sie ein neues letztes Logindatum ein.\n");
						Testkandidat.setLetztesLogindatum(Tastatur.next());
						break;
					case 8:
						System.out.printf("Diese Anwendung wird geschlossen.\n");
						testActive = false;
						break;
					default:
						System.out.printf("Diese Meldung sollte nie angezeigt werden. Der Softwareentwickler sollte den Code �berpr�fen!\n");
				}
			}
			
			System.out.println();  //Abstand schaffen
		}
	}
}

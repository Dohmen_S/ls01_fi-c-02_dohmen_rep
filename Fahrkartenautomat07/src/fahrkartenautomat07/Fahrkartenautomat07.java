//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
//Aufgabe LS-01-14
package fahrkartenautomat07;

import java.util.Scanner;

public class Fahrkartenautomat07 {
	public static int umrechnungZuCent (double wert) {
		wert *= 100;
		int centWert = (int) wert;
		return centWert;
	}
	
	public static double umrechnungZuEuro (int wert) {
		double euroWert = (int) wert;
		euroWert /= 100.0;
		return euroWert;
	}	
	
	public static int fahrkartenBestellung(Scanner eingabe) {
		String fahrscheinListe[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB","Tageskarte Berlin BC", "Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		double preisListe[] = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		int fahrkartenNummer = 0;
		double ticketpreis = 0;
		while (ticketpreis==0) {
			System.out.printf("Es stehen folgende Fahrscheine zur Verf�gung, geben Sie zur Auswahl die Nummer ein:\n");
			for (int i=1; i <= fahrscheinListe.length; i++) {
				System.out.printf("Nr.%-2d %-40s %6.2f Euro\n", i, fahrscheinListe[i-1], preisListe[i-1]);
			}
			fahrkartenNummer = eingabe.nextInt();
			if (fahrkartenNummer > fahrscheinListe.length || fahrkartenNummer <= 0) {
				System.out.println("Diese Karte existiert nicht.");
			}
			else {
				ticketpreis = preisListe[fahrkartenNummer-1];
			}
		}
		System.out.printf("Geben Sie die Anzahl der Tickets ein: \n");
		int anzahlTickets = eingabe.nextInt();
		int zuZahlenderBetrag = umrechnungZuCent(ticketpreis) * anzahlTickets;
		return zuZahlenderBetrag;
	}
	
	public static int fahrkartenBezahlen(Scanner eingabe, int zuZahlenderBetrag) {
        int eingezahlterGesamtbetrag = 0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
    	    System.out.printf("Noch zu zahlen: %.2f Euro\n", umrechnungZuEuro(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	    System.out.printf("Eingabe (mind. 5Ct, h�chstens 2 Euro): \n");
    	    double eingeworfeneM�nze = eingabe.nextDouble();
            eingezahlterGesamtbetrag += umrechnungZuCent(eingeworfeneM�nze);
        }
        return eingezahlterGesamtbetrag;

	}
	
	public static void rueckgeldAusgeben(int eingezahlterGesamtbetrag, int zuZahlenderBetrag) {
		int r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0) {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", umrechnungZuEuro(r�ckgabebetrag));
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 200) {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 200;
	           }
	           while(r�ckgabebetrag >= 100) {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 100;
	           }
	           while(r�ckgabebetrag >= 50) {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 50;
	           }
	           while(r�ckgabebetrag >= 20) {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 20;
	           }
	           while(r�ckgabebetrag >= 10) {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 10;
	           }
	           while(r�ckgabebetrag >= 5) {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 5;
	           }
	           while(r�ckgabebetrag >= 2) {
	        	  System.out.println("2 CENT");
	 	          r�ckgabebetrag -= 2;
	           }
	           while(r�ckgabebetrag >= 1) {
	        	  System.out.println("1 CENT");
	 	          r�ckgabebetrag -= 1;
	           }
	       }
	}
	
	public static void fahrscheinAusgeben () {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++) {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
	}
	
    public static void main(String[] args) {
       Scanner tastatur = new Scanner(System.in);
       int zuZahlenderBetrag; //in Cent
       int eingezahlterBetrag; //in Cent
       
       boolean active = true;
       
       while(active) {	       
	       zuZahlenderBetrag = fahrkartenBestellung(tastatur);
	       
	       if (zuZahlenderBetrag < 0) {
	    	   active = false;
	    	   System.out.println("Automat wird heruntergefahren.");
	       }
	       else {
		       eingezahlterBetrag = fahrkartenBezahlen (tastatur, zuZahlenderBetrag);
		       
		       rueckgeldAusgeben(eingezahlterBetrag, zuZahlenderBetrag);
		       
		       fahrscheinAusgeben ();
	       }
       }
    }
}
//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/

package funktionen;

public class Funktionen {
	public static void main (String[]args)
	{
		double mittel = mittelwert (2,7);
		System.out.printf("%f\n", mittel);
	}


	static double mittelwert (double a, double b) 
	{
		return (a + b)/2.0;
	}
}

/* Funktionen machen den Code lesbarer, helfen bei 
 * Fehlervermeidung, sparen Zeit sowohl beim Code schreiben als auch bei Korrekturen. */

/*Statische Methoden ruft man ohne Objekte auf. Au�erdem besser f�r Speicher.
Nicht statische Methoden ruft man ohne Objekte auf?*/
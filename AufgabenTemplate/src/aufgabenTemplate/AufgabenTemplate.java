//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package aufgabenTemplate;
import java.util.Scanner;

public class AufgabenTemplate {
	
	public static void main (String[]args) {
		String aufgabenWahl = "";
		Scanner Tastatur = new Scanner(System.in);
		boolean programmAktiv = true;
		while (programmAktiv) {
			System.out.println("W�hlen Sie eine Aufgabe aus. M�gliche Eingaben sind: aufgabe1, aufgabe2, aufgabe3. Um das Programm zu beenden geben Sie bitte ende ein.");
			aufgabenWahl = Tastatur.next();
			if (aufgabenWahl.equals("aufgabe1")) {
				System.out.println("");
			}
			else if (aufgabenWahl.equals("aufgabe2")) {
				System.out.println("");
			}
			else if (aufgabenWahl.equals("aufgabe3")) {
				System.out.println("");
			}
			else if (aufgabenWahl.equals("ende")) {
				programmAktiv = false;
				System.out.println("Das Programm wird beendet.");
			}
			else {
				System.out.println("Eingabe unverst�ndlich, bitte wiederholen Sie die Eingabe.");
			}
		}
	}
}

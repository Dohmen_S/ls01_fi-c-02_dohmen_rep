package einfacheArrays;
import java.util.Scanner;
//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
//AB einfache Array Aufgaben 1 bis 3

public class EinfacheArrays {
	
	public static int startZahlErmitteln(Scanner Usereingabe) {
		System.out.print("Bitte geben Sie die erste Zahl ein.\n");
		int start = Usereingabe.nextInt();
		return start;
	}
	
	public static int weitereZahlErmitteln(Scanner Usereingabe) {
		System.out.print("Bitte geben Sie eine weitere Zahl ein.\n");
		int end = Usereingabe.nextInt();
		return end;
	}
	
	public static int endZahlErmitteln(Scanner Usereingabe) {
		System.out.print("Bitte geben Sie die letzte Zahl ein.\n");
		int end = Usereingabe.nextInt();
		return end;
	}
	
	public static void alleZahlen(int start, int end) {
		int[] array = new int[end-start+1];
		
		for(int i=0; i < array.length; i++) {
			array[i] = start+i;
		}
		
		for(int i=0; i < array.length; i++) {
			System.out.print(array[i]);
			if (i != array.length - 1) {
				System.out.print(", ");
			}
			else {
				System.out.print("!\n");
			}
		}
		
		System.out.println("");
	}
	
	public static void ungeradeZahlen(int start, int end) {
		int gerundet = 0;
		int ungeraderStart = 1;
		if ((end-start+1)%2 == 1 && start%2 == 1) {
			gerundet = 1; //+1 falls mehr ungerade Zahlen als gerade
		}
		int[] array = new int[(end-start+1)/2+gerundet];
		
		if (start%2 == 1) {
			ungeraderStart = start;
		}
		else {
			ungeraderStart = start + 1;
		}
		
		for(int i=0; i < array.length; i++) {
			array[i] = ungeraderStart+i*2;
		}
		
		for(int i=0; i < array.length; i++) {
			System.out.print(array[i]);
			if (i != array.length - 1) {
				System.out.print(", ");
			}
			else {
				System.out.print("!\n");
			}
		}
		
		System.out.println("");		
	}
	
	public static void palindrom(int one, int two, int three, int four, int five) {
		int[] array = {one, two, three, four, five};
		
		for (int i=array.length-1; i >= 0; i--) {
			System.out.print(array[i]);
			if (i != 0) {
				System.out.print(", ");
			}
			else {
				System.out.print("!\n");
			}
		}
		
		System.out.println("");
	}
	
	public static void lotto(Scanner Usereingabe) {
		int[] lottoArray = {3, 7, 12, 18, 37, 42};
		boolean zahl12 = false;
		boolean zahl13 = false;
		
		/*startZahlErmitteln(Usereingabe);
		for (int i=1; i < lottoArray.length-1; i--) {
			lottoArray[i] = weitereZahlErmitteln(Usereingabe);
		}
		endZahlErmitteln(Usereingabe);*/
		
		for (int i=0; i < lottoArray.length; i++) {
			System.out.print(lottoArray[i]);
			if (i < lottoArray.length - 1) {
				System.out.print(", ");
			}
			else {
				System.out.print("!\n");
			}
		}
		
		for (int i=0; i < lottoArray.length; i++) {
			if (lottoArray[i] == 12) {
				zahl12 = true;
			}
			if (lottoArray[i] == 13) {
				zahl13 = true;
			}
		}
		
		if (zahl12) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		}
		else {
			System.out.println("Die Zahl 12 ist in der Ziehung nicht enthalten.");
		}
		
		if (zahl13) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		}
		else {
			System.out.println("Die Zahl 13 ist in der Ziehung nicht enthalten.");
		}
		
		System.out.println("");
	}
	
	public static void main (String[]args) {
		Scanner Tastatur = new Scanner(System.in);
		String eingabe;
		boolean active = true;
		while (active) {
			System.out.print("Bitte entscheiden Sie sich f�r eine der folgenden Optionen:\n");
			System.out.print("F�r alle Zahlen ausgeben, bitte alle eingeben.\n"
					+ "F�r nur ungerade Zahlen ausgeben, bitte ungerade eingeben.\n"
					+ "Um 5 Zahlen in umgekehrter Reihenfolge auszugeben bitte palindrom eingeben.\n"
					+ "Um die Lottozahlen auszuweisen, geben Sie bitte lotto ein.\n"
					+ "F�r beenden bitte ende eingeben.\n");
			eingabe = Tastatur.next();
			if (eingabe.equals("alle") || eingabe.equals("ALLE")) {
				alleZahlen(startZahlErmitteln(Tastatur), endZahlErmitteln(Tastatur));
			}
			else if (eingabe.equals("ungerade") || eingabe.equals("UNGERADE")) {
				ungeradeZahlen(startZahlErmitteln(Tastatur), endZahlErmitteln(Tastatur));
			}
			else if (eingabe.equals("palindrom") || eingabe.equals("PALINDROM")) {
				palindrom(startZahlErmitteln(Tastatur), weitereZahlErmitteln(Tastatur), weitereZahlErmitteln(Tastatur), weitereZahlErmitteln(Tastatur), endZahlErmitteln(Tastatur));;
			}
			else if (eingabe.equals("lotto") || eingabe.equals("LOTTO")) {
				lotto(Tastatur);;
			}
			else if (eingabe.equals("ende") || eingabe.equals("ENDE")) {
				active = false;
			}
			else {
				System.out.print("Sie haben etwas anderes eingegeben, bitte wiederholen Sie ihre Eingabe.\n");
			}
		}
		System.out.print("Programm wird beendet.");
	}

}
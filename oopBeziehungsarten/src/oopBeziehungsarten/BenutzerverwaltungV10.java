package oopBeziehungsarten;
import java.io.InputStream;
import java.util.Scanner;
import java.io.Console;

public class BenutzerverwaltungV10 {

    public static void main(String[] args) {

        Benutzerverwaltung.start();

    }
}

class Benutzerverwaltung{
    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        //System.out.println(benutzerListe.select());
        //System.out.println(benutzerListe.select("Peter"));
        //System.out.println(benutzerListe.select("Darko"));
        
        System.out.println(benutzerListe.select("Paula"));
        benutzerListe.delete("Paula");
        benutzerListe.delete("Paula");
        System.out.println(benutzerListe.select("Paula"));
        
        System.out.println(benutzerListe.select("Adam37"));
        benutzerListe.delete("Adam37");
        benutzerListe.delete("Adam37");
        System.out.println(benutzerListe.select("Adam37"));
        
        System.out.println(benutzerListe.select("Darko"));
        benutzerListe.delete("Darko");
        benutzerListe.delete("Darko");
        System.out.println(benutzerListe.select("Darko"));
        
        
		String menueWahl = "";
		Scanner Tastatur = new Scanner(System.in);
		boolean menueAktiv = true;
		final String menueEintraege[] = {"Anmelden", "Registrieren", "Beenden"};
		while (menueAktiv) {
			System.out.printf("Sie sind im Hauptmen�.\n\n");
			System.out.printf("W�hlen Sie eine Aktion aus. M�gliche Eingaben sind:\n\n");
			for(int i=0; i < menueEintraege.length; i++) {
				System.out.printf("%d %s\n", i+1,menueEintraege[i]);
				if(i == menueEintraege.length-1) {
					System.out.println("");
				}
			}
			
			menueWahl = Tastatur.next();
			
			if(menueWahl.equals("Anmelden")) {
				benutzerListe.anmelden(Tastatur);
			}	
			
			else if(menueWahl.equals("Registrieren")) {
				benutzerListe.registrieren(Tastatur);
			}
			
			else if(menueWahl.equals("Beenden")) {
				menueAktiv = false;
				System.out.printf("Das Programm wird beendet.\n");
			}
			
			else {
				System.out.println("Eingabe unverst�ndlich, bitte wiederholen Sie die Eingabe.");		
			}
		}
    }
}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public void delete(String name){
    	Benutzer b = first;
    	boolean deleteSearchStatus = true;
    	boolean deleted = false;
    	
		if(first!=null && b.getName().equals(name)) {
			if(b.getNext() != null) {
				first = b.getNext();
				b.setNext(null);
			}
			else {
				first = null;
				last = null;
			}
			deleteSearchStatus = false;
			deleted = true;
		}
    	
    	while(first!=null && deleteSearchStatus && last!=first) {
    		if(b.getNext().getName().equals(name)) {
    			if(last == b.getNext()) {
    				last = b;
    			}
    			
    			Benutzer next = b.getNext().getNext();
    			b.getNext().setNext(null);
    			b.setNext(next);
    			deleteSearchStatus = false;
    			deleted = true;
    		}
    		
    		b = b.getNext();
    		if (deleteSearchStatus == true) {
	    		if(b.getNext() == null) {
	    			deleteSearchStatus = false;
	    		}
    		}
    	}
    	
        if(deleted == true) {
        	System.out.printf("%s wurde gel�scht.\n", name);
        }
        else {
        	System.out.printf("%s konnte nicht gefunden werden.\n", name);
        }
    }
    
    public void anmelden(Scanner Nutzereingabe) {
    	String anmeldenBenutzername;
    	String anmeldenPasswort;
    	Benutzer b = first;
    	boolean searchStatus = true;
    	
    	System.out.printf("Sie sind im Anmeldemen�.\n");
    	System.out.printf("Bitte geben Sie ihren Benutzernamen ein.\n");
    	anmeldenBenutzername = Nutzereingabe.next();
    	System.out.printf("Bitte geben Sie ihr Passwort ein.\n");
    	anmeldenPasswort =  Nutzereingabe.next();
    	
    	while(searchStatus==true) {
    		if(b != null) {
	    		if(b.hasName(anmeldenBenutzername)) {
	    			if(b.getPasswort().equals(anmeldenPasswort)) {
	    				System.out.printf("Sie sind nun als %s angemeldet.\n\n", b.getName());
	    				searchStatus = false;
	    			}
	    			else {
	    				System.out.printf("Sie haben das Passwort falsch eingegeben.\n\n");
	    				searchStatus = false;
	    			}
	    		}
	    		
	        	if(b.getNext() == null && searchStatus == true) {
	        		System.out.printf("Dieser Benutzer existiert nicht.\n\n");
	        		searchStatus = false;
	        	}
	        	else {
	        		b = b.getNext();
	    		}
	    	}
    		else {
    			searchStatus = false;
    			System.out.printf("Dieser Benutzer existiert nicht.\n\n");
    		}
    	}
    }
    
    public void registrieren(Scanner Nutzereingabe) {
    	String registrierenBenutzername;
    	String registrierenPasswort;
    	String registrierenPasswortWiederholung;
    	Benutzer b = first;
    	boolean searchStatus = true;
    	boolean registrierungsVorgang = true;
    	
    	System.out.printf("Sie sind in der Registrierung.\n");
    	System.out.printf("Bitte geben Sie ihren gew�nschten Benutzernamen ein.\n");
    	registrierenBenutzername = Nutzereingabe.next();
    	if(b != null) {
	    	while(searchStatus==true) {
	    		if(b.hasName(registrierenBenutzername)) {
	    			searchStatus = false;
	    			registrierungsVorgang = false;
	    			System.out.printf("Dieser Benutzername ist bereits vergeben.\n");
	    		}
	    		
	        	if(b.getNext() == null) {
	        		searchStatus = false;
	        	}
	        	else {
	        		b = b.getNext();
	    		}
	    	}
    	}
    	
    	if(registrierungsVorgang == true) {
	    	System.out.printf("Bitte geben Sie ihr gew�nschtes Passwort ein.\n");
	    	registrierenPasswort = Nutzereingabe.next();
	    	System.out.printf("Bitte wiederholen Sie ihr gew�nschtes Passwort.\n");
	    	registrierenPasswortWiederholung = Nutzereingabe.next();
	    	
	    	if(registrierenPasswortWiederholung.equals(registrierenPasswort)) {
	    		Benutzer neuerBenutzer = new Benutzer(registrierenBenutzername, registrierenPasswort);
	    		this.insert(neuerBenutzer);
	    		System.out.printf("Sie sind nun unter dem Namen %s registriert\n", registrierenBenutzername);
	    	}
	    	else {
	    		System.out.printf("Die Passw�rter stimmen nicht �berein.\n");
	    	}
    	}
    }
}

class Benutzer{
    private String name;
    private String passwort;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }
    
    public String getPasswort() {
    	return passwort;
    }
    
    public String getName() {
    	return name;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}





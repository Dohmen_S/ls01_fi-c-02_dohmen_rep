//Sergey Dohmen und Josua Kawgan-Kagan
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
package roemischeZahlen1;
import java.util.Scanner;

public class RoemischeZahlen1 {

	public static void main(String[] args) {
		System.out.printf("Bitte geben Sie eine r�mische Zahl bestehend aus einem zeichen ein.\n");
		Scanner Tastatur = new Scanner(System.in);
		
		String roemZahl = Tastatur.next(); //.next statt .nextString
		int roemDezimal = roemZuDezimal(roemZahl);
		if (roemDezimal!=0)
			System.out.printf("Diese r�mische Zahl ist eine %d\n", roemDezimal);
		else
			System.out.printf("Das ist keine R�mische Zahl.\n");

	}
	
	static int roemZuDezimal(String roemZahl) {
		/*String i = "I";
		String v = "V";
		String x = "X";
		String l = "L";
		String c = "C";
		String d = "D";
		String m = "M";*/
		if (roemZahl.equals("I"))
			return 1;
		else if (roemZahl.equals("V"))
			return 5;
		else if (roemZahl.equals("X"))
			return 10;
		else if (roemZahl.equals("C"))
			return 50;
		else if (roemZahl.equals("L"))
			return 100;
		else if (roemZahl.equals("D"))
			return 500;
		else if (roemZahl.equals("M"))
			return 1000;
		else
			System.out.printf("return 0\n");
			return 0;
	}

}
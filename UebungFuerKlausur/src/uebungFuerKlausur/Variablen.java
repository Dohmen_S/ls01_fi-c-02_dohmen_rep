//Sergey Dohmen
//https://bitbucket.org/Dohmen_S/ls01_fi-c-02_dohmen_rep/src/master/
//Version 2.0

package uebungFuerKlausur;

import java.util.Scanner;

public class Variablen {
	public static void main(String []args) {
		int zaehler = 0;
		int aufgabenNummer = 99;
		char menuePunkt = 'A';
		while (aufgabenNummer!=0)
		{
			System.out.println("Welche Aufgabe wollen Sie pr�fen? Geben Sie eine Zahl von 1-11 ein. Geben Sie eine 0 zum beenden ein.");
			Scanner Tastatur = new Scanner(System.in);
			aufgabenNummer = Tastatur.nextInt();
		    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
	          Vereinbaren Sie eine geeignete Variable */
			if (aufgabenNummer==1)
				while (zaehler<20) {
					zaehler += 1;
					System.out.printf("Der Zaehler ist nun bei %d\n", zaehler);
				}
		    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
	          und geben Sie ihn auf dem Bildschirm aus.*/
			else if (aufgabenNummer==2)
			{
				zaehler = 25;
				System.out.println(zaehler);
			}
		    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
	          eines Programms ausgewaehlt werden.
	          Vereinbaren Sie eine geeignete Variable */
			else if (aufgabenNummer==3)
			{
				System.out.printf("Der derzeitige Men�punkt ist: %s. Geben Sie einen Buchstaben ein um den Men�punkt zu wechseln.\n", menuePunkt);
				System.out.printf("Der neue Men�punkt ist: ");
			}
		}
	}
}

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/

